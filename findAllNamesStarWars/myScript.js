let countNames = 0;
let countPages = 0;

function sendRequest(url) {
  //# send request

  if (url == undefined) {
    url = "https://swapi.dev/api/people/";
  }

  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", url, true);
  xhttp.onload = handleResponse;
  xhttp.send();

  //# let the user know
  document.getElementById("demo").innerHTML = "<h2>Request send</h2>";
  //# disable button
  document.getElementById("btn").disabled = true;
}

function handleResponse() {
  //# handle response good & bad
  if (this.readyState == 4 && this.status == 200) {
    //# good response
    let myObj = JSON.parse(this.responseText);
    console.log("Showing the object:", myObj);
    console.log(myObj.results);
    console.log(myObj.next);
    let nameElement = document.querySelector(".names");

    myObj.results.forEach((element) => {
      //   console.log(element.name);
      countNames++;
      nameElement.innerHTML += `<p>${element.name}</p>`;
    });
    if (myObj.next != null) {
      countPages++;
      sendRequest(myObj.next);
      console.log(countNames);
    } else {
      console.log(countNames);
      console.log(countPages + 1);
    }
  } else {
    //# bad response
    demoDiv.innerHTML =
      "An error occured!<br>status: " +
      this.status +
      "<br>readyState: " +
      this.readyState;
  }
  //# reactivate button
  document.getElementById("btn").disabled = false;
}
